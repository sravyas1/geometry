public class Quadrilateral extends Shape {
	
	Quadrilateral() {
		super("Quadrilateral") ;
	}
	String getName() {
		return name;
	}
	double perimeter() {
		return 5;
	}
	double area() {
		return 10;
	}
	public static void main(String[] args) {
		Quadrilateral q1 = new Quadrilateral();
		System.out.println(q1.getName()+q1.area()) ;
	}
	
}

