public abstract class Shape {
	String name;
	Shape(String nm) {
		name = nm;
	}
	String getName() {
		return name;
	}
	abstract double perimeter() ;
	abstract double area() ;
	
}

