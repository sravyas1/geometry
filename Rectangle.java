public class Rectangle extends Shape {
	double length;
	double width;
	Rectangle( double l, double w) {
		super("Rectangle") ;
		length = l;
		width = w;
	}
	Rectangle() { 
		this(10,10);
	}
	double perimeter() {
		return 2*(length + width);
	}
	double area() {
		return length * width;
	}
	public static void main(String[] args) {
		Rectangle r1 = new Rectangle(5,7);
		Rectangle r2 = new Rectangle();
		System.out.println(r1.getName()+r1.area()+" "+r2.perimeter()) ;
	}
	
}

