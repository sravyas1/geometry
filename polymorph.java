public class Polymorph {
	public static void main(String[] args) {
		if (args.length == 0 ) 
			System.exit(0);
		int cla = Integer.parseInt(args[0]);
		Shape sp ;
		Rectangle r1 = new Rectangle(5,7);
		Square s1 = new Square(13);
		Circle c1 = new Circle(17);
		if (cla == 1) 
			sp = s1;
		else if (cla == 2)
			sp = r1;
		else 
			sp = c1;		
		System.out.println(sp.getName()+" with area "+sp.area()+" and perimeter "+sp.perimeter()) ;
	}
	
}

