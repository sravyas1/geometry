public class Polymorph2 {
	public static void main(String[] args) {
		Shape sp ;
		Rectangle r1 = new Rectangle(5,7);
		Square s1 = new Square(13);
		Circle c1 = new Circle(17);
		double rn = Math.random() ;
		if (rn < .3) 
			sp = s1;
		else if (rn < .66)
			sp = r1;
		else 
			sp = c1;		
		System.out.println(sp.getName()+" with area "+sp.area()+" and perimeter "+sp.perimeter()) ;
	}
}

