public class Square extends Rectangle {
	double side;
	Square( double l) {
		super(l,l) ;
		side=l;
	}
	Square() { 
		this(10);
	}
	double perimeter() {
		return 4 * side;
	}
	double area() {
		return side*side;
	}
	public static void main(String[] args) {
		Square s1 = new Square(7);
		Square s2 = new Square();
		System.out.println(s1.getName()+s1.area()+" "+s2.perimeter()) ;
	}
	
}

