public class Circle extends Shape {
	double radius;
	Circle( double r) {
		super("Circle") ;
		radius = r;
	}
	Circle() { 
		this(10);
	}
	double perimeter() {
		return 2*Math.PI*radius;
	}
	double area() {
		return Math.PI * radius * radius;
	}
	public static void main(String[] args) {
		Circle c1 = new Circle(7);
		Circle c2 = new Circle();
		System.out.println(c1.getName()+c1.area()+" "+c2.perimeter()) ;
	}
	
}

